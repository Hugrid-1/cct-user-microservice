package storage

import (
	"context"
	"gitlab.com/Hugrid-1/cct-user-microservice/internal/db/adapter"
	"gitlab.com/Hugrid-1/cct-user-microservice/public/models"
)

type Userer interface {
	Create(ctx context.Context, u models.UserDTO) (int, error)
	Update(ctx context.Context, u models.UserDTO) error
	GetByID(ctx context.Context, userID int) (models.UserDTO, error)
	GetByIDs(ctx context.Context, ids []int) ([]models.UserDTO, error)
	GetByEmail(ctx context.Context, email string) (models.UserDTO, error)
	GetByFilter(ctx context.Context, condition adapter.Condition) ([]models.UserDTO, error)
}
