package service

import (
	"context"
	"github.com/lib/pq"
	"gitlab.com/Hugrid-1/cct-user-microservice/internal/infrastructure/errors"
	"gitlab.com/Hugrid-1/cct-user-microservice/internal/infrastructure/tools/cryptography"
	"gitlab.com/Hugrid-1/cct-user-microservice/internal/modules/user/storage"
	models2 "gitlab.com/Hugrid-1/cct-user-microservice/public/models"
	"gitlab.com/Hugrid-1/cct-user-microservice/public/user_interface"
	"go.uber.org/zap"
)

type UserService struct {
	storage storage.Userer
	logger  *zap.Logger
}

func NewUserService(storage storage.Userer, logger *zap.Logger) *UserService {
	return &UserService{storage: storage, logger: logger}
}

func (u *UserService) Create(ctx context.Context, in user_interface.UserCreateIn) user_interface.UserCreateOut {
	var dto models2.UserDTO
	dto.SetName(in.Name).
		SetPhone(in.Phone).
		SetEmail(in.Email).
		SetPassword(in.Password).
		SetRole(in.Role)

	userID, err := u.storage.Create(ctx, dto)
	if err != nil {
		if v, ok := err.(*pq.Error); ok && v.Code == "23505" {
			return user_interface.UserCreateOut{
				ErrorCode: errors.UserServiceUserAlreadyExists,
			}
		}
		return user_interface.UserCreateOut{
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}

	return user_interface.UserCreateOut{
		UserID: userID,
	}
}

func (u *UserService) Update(ctx context.Context, in user_interface.UserUpdateIn) user_interface.UserUpdateOut {
	panic("implement me")
}

func (u *UserService) VerifyEmail(ctx context.Context, in user_interface.UserVerifyEmailIn) user_interface.UserUpdateOut {
	dto, err := u.storage.GetByID(ctx, in.UserID)
	if err != nil {
		u.logger.Error("user: GetByEmail err", zap.Error(err))
		return user_interface.UserUpdateOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}
	dto.SetEmailVerified(true)
	err = u.storage.Update(ctx, dto)
	if err != nil {
		u.logger.Error("user: update err", zap.Error(err))
		return user_interface.UserUpdateOut{
			ErrorCode: errors.UserServiceUpdateErr,
		}
	}

	return user_interface.UserUpdateOut{
		Success: true,
	}
}

func (u *UserService) ChangePassword(ctx context.Context, in user_interface.ChangePasswordIn) user_interface.ChangePasswordOut {
	dto, err := u.storage.GetByID(ctx, in.UserID)

	oldHashedPassword, err := cryptography.HashPassword(in.OldPassword)
	_ = oldHashedPassword
	if !cryptography.CheckPassword(dto.GetPassword(), in.OldPassword) {
		return user_interface.ChangePasswordOut{
			ErrorCode: errors.UserServiceChangePasswordErr,
		}
	}

	newHashedPassword, err := cryptography.HashPassword(in.NewPassword)
	dto.SetPassword(newHashedPassword)

	if err = u.storage.Update(ctx, dto); err != nil {
		u.logger.Error("user: update err", zap.Error(err))
		return user_interface.ChangePasswordOut{
			ErrorCode: errors.UserServiceChangePasswordErr,
		}
	}

	return user_interface.ChangePasswordOut{
		Success:   true,
		ErrorCode: 0,
	}
}

func (u *UserService) GetByEmail(ctx context.Context, in user_interface.GetByEmailIn) user_interface.UserOut {
	userDTO, err := u.storage.GetByEmail(ctx, in.Email)
	if err != nil {
		u.logger.Error("user: GetByEmail err", zap.Error(err))
		return user_interface.UserOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}

	return user_interface.UserOut{
		User: &models2.User{
			ID:            userDTO.GetID(),
			Name:          userDTO.GetName(),
			Phone:         userDTO.GetPhone(),
			Email:         userDTO.GetEmail(),
			Password:      userDTO.GetPassword(),
			Role:          userDTO.GetRole(),
			Verified:      userDTO.Verified,
			EmailVerified: userDTO.EmailVerified,
			PhoneVerified: userDTO.PhoneVerified,
		},
	}
}

func (u *UserService) GetByPhone(ctx context.Context, in user_interface.GetByPhoneIn) user_interface.UserOut {
	panic("implement me")
}

func (u *UserService) GetByID(ctx context.Context, in user_interface.GetByIDIn) user_interface.UserOut {
	userDTO, err := u.storage.GetByID(ctx, in.UserID)
	if err != nil {
		u.logger.Error("user: GetByEmail err", zap.Error(err))
		return user_interface.UserOut{
			ErrorCode: errors.UserServiceRetrieveUserErr,
		}
	}

	return user_interface.UserOut{
		User: &models2.User{
			ID:            userDTO.GetID(),
			Name:          userDTO.GetName(),
			Phone:         userDTO.GetPhone(),
			Email:         userDTO.GetEmail(),
			Password:      userDTO.GetPassword(),
			Role:          userDTO.Role,
			Verified:      userDTO.Verified,
			EmailVerified: userDTO.EmailVerified,
			PhoneVerified: userDTO.PhoneVerified,
		},
	}
}

func (u *UserService) GetByIDs(ctx context.Context, in user_interface.GetByIDsIn) user_interface.UsersOut {
	panic("implement me")
}
