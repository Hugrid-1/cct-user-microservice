package modules

import (
	"gitlab.com/Hugrid-1/cct-user-microservice/internal/infrastructure/component"
	uservice "gitlab.com/Hugrid-1/cct-user-microservice/internal/modules/user/service"
	"gitlab.com/Hugrid-1/cct-user-microservice/internal/storages"
	"gitlab.com/Hugrid-1/cct-user-microservice/public/user_interface"
)

type Services struct {
	User user_interface.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
	}
}
