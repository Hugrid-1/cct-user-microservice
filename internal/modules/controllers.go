package modules

import (
	"gitlab.com/Hugrid-1/cct-user-microservice/internal/infrastructure/component"
	ucontroller "gitlab.com/Hugrid-1/cct-user-microservice/internal/modules/user/controller"
)

type Controllers struct {
	User ucontroller.Userer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	userController := ucontroller.NewUser(services.User, components)

	return &Controllers{
		User: userController,
	}
}
