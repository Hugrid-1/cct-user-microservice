package storages

import (
	"gitlab.com/Hugrid-1/cct-user-microservice/internal/db/adapter"
	"gitlab.com/Hugrid-1/cct-user-microservice/internal/infrastructure/cache"
	ustorage "gitlab.com/Hugrid-1/cct-user-microservice/internal/modules/user/storage"
)

type Storages struct {
	User ustorage.Userer
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User: ustorage.NewUserStorage(sqlAdapter, cache),
	}
}
