package jrpc

import (
	"context"
	"gitlab.com/Hugrid-1/cct-user-microservice/public/user_interface"
)

// UserJRPCServer представляет UserService для использования в JSON-RPC
type UserJRPCServer struct {
	userService user_interface.Userer
}

// NewUserJRPCServer возвращает новый UserJRPCServer
func NewUserJRPCServer(userService user_interface.Userer) *UserJRPCServer {
	return &UserJRPCServer{userService: userService}
}

// CreateUser обрабатывает JSON-RPC запрос на создание пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод CreateUser из userService.
// Возвращает выходные данные.
func (t *UserJRPCServer) CreateUser(in user_interface.UserCreateIn, out *user_interface.UserCreateOut) error {
	*out = t.userService.Create(context.Background(), in)
	return nil
}

// UpdateUser обрабатывает JSON-RPC запрос на обновление пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод Update из userService.
// Возвращает выходные данные.
func (t *UserJRPCServer) UpdateUser(in user_interface.UserUpdateIn, out *user_interface.UserUpdateOut) error {
	*out = t.userService.Update(context.Background(), in)
	return nil
}

// VerifyEmail обрабатывает JSON-RPC запрос на подтверждение email пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод VerifyEmail из userService.
// Возвращает выходные данные.
func (t *UserJRPCServer) VerifyEmail(in user_interface.UserVerifyEmailIn, out *user_interface.UserUpdateOut) error {
	*out = t.userService.VerifyEmail(context.Background(), in)
	return nil
}

// ChangePassword обрабатывает JSON-RPC запрос на смену пароля пользователя.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод ChangePassword из userService.
// Возвращает выходные данные.
func (t *UserJRPCServer) ChangePassword(in user_interface.ChangePasswordIn, out *user_interface.ChangePasswordOut) error {
	*out = t.userService.ChangePassword(context.Background(), in)
	return nil
}

// GetUserByEmail обрабатывает JSON-RPC запрос на получение пользователя по email.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод GetByEmail из userService.
// Возвращает выходные данные.
func (t *UserJRPCServer) GetUserByEmail(in user_interface.GetByEmailIn, out *user_interface.UserOut) error {
	*out = t.userService.GetByEmail(context.Background(), in)
	return nil
}

// GetUserByPhone обрабатывает JSON-RPC запрос на получение пользователя по телефону.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод GetByPhone из userService.
// Возвращает выходные данные.
func (t *UserJRPCServer) GetUserByPhone(in user_interface.GetByPhoneIn, out *user_interface.UserOut) error {
	*out = t.userService.GetByPhone(context.Background(), in)
	return nil
}

// GetUserByID обрабатывает JSON-RPC запрос на получение пользователя по ID.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод GetByID из userService.
// Возвращает выходные данные.
func (t *UserJRPCServer) GetUserByID(in user_interface.GetByIDIn, out *user_interface.UserOut) error {
	*out = t.userService.GetByID(context.Background(), in)
	return nil
}

// GetUsersByID обрабатывает JSON-RPC запрос на получение пользователей по списку ID.
// Принимает входные данные, использует контекст по умолчанию и вызывает метод
// GetByIDs из userService. Возвращает выходные данные.
func (t *UserJRPCServer) GetUsersByID(in user_interface.GetByIDsIn, out *user_interface.UsersOut) error {
	*out = t.userService.GetByIDs(context.Background(), in)
	return nil
}
