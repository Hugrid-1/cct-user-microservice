package jrpc

import (
	"context"
	"gitlab.com/Hugrid-1/cct-user-microservice/internal/infrastructure/errors"
	"gitlab.com/Hugrid-1/cct-user-microservice/public/user_interface"
	"net/rpc"
)

type UserJRPCClient struct {
	client *rpc.Client
}

func NewUserJRPCClient(serverAddress string) (*UserJRPCClient, error) {
	client, err := rpc.Dial("tcp", serverAddress)
	if err != nil {
		return nil, err
	}

	return &UserJRPCClient{client: client}, nil
}

func (t *UserJRPCClient) Create(ctx context.Context, in user_interface.UserCreateIn) user_interface.UserCreateOut {
	var out user_interface.UserCreateOut
	err := t.client.Call("UserJRPCServer.CreateUser", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceCreateUserErr
	}

	return out
}

func (t *UserJRPCClient) Update(ctx context.Context, in user_interface.UserUpdateIn) user_interface.UserUpdateOut {
	var out user_interface.UserUpdateOut
	err := t.client.Call("UserJRPCServer.UpdateUser", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceUpdateErr
	}

	return out
}

func (t *UserJRPCClient) VerifyEmail(ctx context.Context, in user_interface.UserVerifyEmailIn) user_interface.UserUpdateOut {
	var out user_interface.UserUpdateOut
	err := t.client.Call("UserJRPCServer.VerifyEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceUpdateErr
	}

	return out
}

func (t *UserJRPCClient) ChangePassword(ctx context.Context, in user_interface.ChangePasswordIn) user_interface.ChangePasswordOut {
	var out user_interface.ChangePasswordOut
	err := t.client.Call("UserJRPCServer.ChangePassword", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceChangePasswordErr
	}

	return out
}

func (t *UserJRPCClient) GetByEmail(ctx context.Context, in user_interface.GetByEmailIn) user_interface.UserOut {
	var out user_interface.UserOut
	err := t.client.Call("UserJRPCServer.GetUserByEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUserErr
	}

	return out
}

func (t *UserJRPCClient) GetByPhone(ctx context.Context, in user_interface.GetByPhoneIn) user_interface.UserOut {
	var out user_interface.UserOut
	err := t.client.Call("UserJRPCServer.GetUserByPhone", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUserErr
	}

	return out
}

func (t *UserJRPCClient) GetByID(ctx context.Context, in user_interface.GetByIDIn) user_interface.UserOut {
	var out user_interface.UserOut
	err := t.client.Call("UserJRPCServer.GetUserByID", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUserErr
	}

	return out
}

func (t *UserJRPCClient) GetByIDs(ctx context.Context, in user_interface.GetByIDsIn) user_interface.UsersOut {
	var out user_interface.UsersOut
	err := t.client.Call("UserJRPCServer.GetUsersByID", in, &out)
	if err != nil {
		out.ErrorCode = errors.UserServiceRetrieveUsersErr
	}

	return out
}
