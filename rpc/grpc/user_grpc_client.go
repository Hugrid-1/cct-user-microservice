package grpc

import (
	"context"
	"gitlab.com/Hugrid-1/cct-user-microservice/internal/infrastructure/errors"
	"gitlab.com/Hugrid-1/cct-user-microservice/public/models"
	"gitlab.com/Hugrid-1/cct-user-microservice/public/user_interface"
	"google.golang.org/grpc/credentials/insecure"
	"log"

	pb "gitlab.com/Hugrid-1/cct-user-microservice/rpc/grpc/protobuf"

	"google.golang.org/grpc"
)

type UserGRPCClient struct {
	client pb.UserClient
}

func NewUserGRPCClient(address string) (*UserGRPCClient, error) {
	conn, err := grpc.Dial(address, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	client := pb.NewUserClient(conn)
	return &UserGRPCClient{
		client: client,
	}, nil
}

func (c *UserGRPCClient) Create(ctx context.Context, in user_interface.UserCreateIn) user_interface.UserCreateOut {
	request := &pb.UserCreateRequest{
		Name:     in.Name,
		Phone:    in.Phone,
		Email:    in.Email,
		Password: in.Password,
		Role:     int32(in.Role),
	}

	response, err := c.client.CreateUser(ctx, request)
	if err != nil {
		log.Fatal(err)
	}

	return user_interface.UserCreateOut{
		UserID:    int(response.UserID),
		ErrorCode: int(response.ErrorCode),
	}
}

func (c *UserGRPCClient) Update(ctx context.Context, in user_interface.UserUpdateIn) user_interface.UserUpdateOut {
	request := &pb.UserUpdateRequest{
		User: &pb.UserStruct{
			ID:            int32(in.User.ID),
			Name:          in.User.Name,
			Phone:         in.User.Phone,
			Email:         in.User.Email,
			Password:      in.User.Password,
			Role:          int32(in.User.Role),
			Verified:      in.User.Verified,
			EmailVerified: in.User.EmailVerified,
			PhoneVerified: in.User.PhoneVerified,
		},
	}

	response, err := c.client.UpdateUser(ctx, request)
	if err != nil {
		return user_interface.UserUpdateOut{
			ErrorCode: errors.UserServiceCreateUserErr,
		}
	}

	return user_interface.UserUpdateOut{
		Success:   response.Success,
		ErrorCode: int(response.ErrorCode),
	}
}

func (c *UserGRPCClient) VerifyEmail(ctx context.Context, in user_interface.UserVerifyEmailIn) user_interface.UserUpdateOut {
	request := &pb.VerifyEmailRequest{
		UserID: int32(in.UserID),
	}

	response, err := c.client.VerifyEmail(ctx, request)
	if err != nil {
		return user_interface.UserUpdateOut{ErrorCode: errors.AuthServiceVerifyErr}
	}

	return user_interface.UserUpdateOut{
		Success:   response.Success,
		ErrorCode: int(response.ErrorCode),
	}
}

func (c *UserGRPCClient) ChangePassword(ctx context.Context, in user_interface.ChangePasswordIn) user_interface.ChangePasswordOut {
	request := &pb.ChangePasswordRequest{
		UserID:      int32(in.UserID),
		OldPassword: in.OldPassword,
		NewPassword: in.NewPassword,
	}

	response, err := c.client.UserChangePassword(ctx, request)
	if err != nil {
		return user_interface.ChangePasswordOut{ErrorCode: errors.UserServiceChangePasswordErr}
	}

	return user_interface.ChangePasswordOut{
		Success:   response.Success,
		ErrorCode: int(response.ErrorCode),
	}
}

func (c *UserGRPCClient) GetByEmail(ctx context.Context, in user_interface.GetByEmailIn) user_interface.UserOut {
	request := &pb.GetByEmailRequest{
		Email: in.Email,
	}

	response, err := c.client.GetUserByEmail(ctx, request)
	if err != nil {
		return user_interface.UserOut{ErrorCode: errors.UserServiceRetrieveUserErr}
	}

	return user_interface.UserOut{
		User: &models.User{
			ID:            int(response.User.ID),
			Name:          response.User.Name,
			Phone:         response.User.Phone,
			Email:         response.User.Email,
			Password:      response.User.Password,
			Role:          int(response.User.Role),
			Verified:      response.User.Verified,
			EmailVerified: response.User.EmailVerified,
			PhoneVerified: response.User.PhoneVerified,
		},
		ErrorCode: int(response.ErrorCode),
	}
}

func (c *UserGRPCClient) GetByPhone(ctx context.Context, in user_interface.GetByPhoneIn) user_interface.UserOut {
	request := &pb.GetByPhoneRequest{
		Phone: in.Phone,
	}

	response, err := c.client.GetUserByPhone(ctx, request)
	if err != nil {
		return user_interface.UserOut{ErrorCode: errors.UserServiceRetrieveUserErr}
	}

	return user_interface.UserOut{
		User: &models.User{
			ID:            int(response.User.ID),
			Name:          response.User.Name,
			Phone:         response.User.Phone,
			Email:         response.User.Email,
			Password:      response.User.Password,
			Role:          int(response.User.Role),
			Verified:      response.User.Verified,
			EmailVerified: response.User.EmailVerified,
			PhoneVerified: response.User.PhoneVerified,
		},
		ErrorCode: int(response.ErrorCode),
	}
}

func (c *UserGRPCClient) GetByID(ctx context.Context, in user_interface.GetByIDIn) user_interface.UserOut {
	request := &pb.GetByIDRequest{
		ID: int32(in.UserID),
	}

	response, err := c.client.GetUserByID(ctx, request)
	if err != nil {
		log.Fatal(err)
	}

	return user_interface.UserOut{
		User: &models.User{
			ID:            int(response.User.ID),
			Name:          response.User.Name,
			Phone:         response.User.Phone,
			Email:         response.User.Email,
			Password:      response.User.Password,
			Role:          int(response.User.Role),
			Verified:      response.User.Verified,
			EmailVerified: response.User.EmailVerified,
			PhoneVerified: response.User.PhoneVerified,
		},

		ErrorCode: int(response.ErrorCode),
	}
}

func (c *UserGRPCClient) GetByIDs(ctx context.Context, in user_interface.GetByIDsIn) user_interface.UsersOut {
	request := &pb.GetByIDsRequest{
		IDs: make([]int32, len(in.UserIDs)),
	}

	for i, id := range in.UserIDs {
		request.IDs[i] = int32(id)
	}

	response, err := c.client.GetUsersByIDs(ctx, request)
	if err != nil {
		log.Fatal(err)
	}

	users := make([]models.User, len(response.User))
	for i, u := range response.User {
		users[i] = models.User{
			ID:            int(u.ID),
			Name:          u.Name,
			Phone:         u.Phone,
			Email:         u.Email,
			Password:      u.Password,
			Role:          int(u.Role),
			Verified:      u.Verified,
			EmailVerified: u.EmailVerified,
			PhoneVerified: u.PhoneVerified,
		}
	}

	return user_interface.UsersOut{
		User:      users,
		ErrorCode: int(response.ErrorCode),
	}
}
