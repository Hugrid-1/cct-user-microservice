package grpc

import (
	"context"
	"gitlab.com/Hugrid-1/cct-user-microservice/public/user_interface"
	pb "gitlab.com/Hugrid-1/cct-user-microservice/rpc/grpc/protobuf"
)

type UserGRPCServer struct {
	userService user_interface.Userer
	pb.UnimplementedUserServer
}

func NewUserGRPCServer(userService user_interface.Userer) *UserGRPCServer {
	return &UserGRPCServer{
		userService: userService,
	}
}

func (u UserGRPCServer) CreateUser(ctx context.Context, in *pb.UserCreateRequest) (*pb.UserCreateResponse, error) {
	out := u.userService.Create(ctx, user_interface.UserCreateIn{
		Name:     in.GetName(),
		Phone:    in.GetPhone(),
		Email:    in.GetEmail(),
		Password: in.GetPassword(),
		Role:     int(in.GetRole()),
	})

	return &pb.UserCreateResponse{
		UserID:    int32(out.UserID),
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u UserGRPCServer) GetUserByEmail(ctx context.Context, in *pb.GetByEmailRequest) (*pb.UserOutResponse, error) {
	out := u.userService.GetByEmail(ctx, user_interface.GetByEmailIn{
		Email: in.GetEmail(),
	})

	return &pb.UserOutResponse{
		User: &pb.UserStruct{
			ID:            int32(out.User.ID),
			Name:          out.User.Name,
			Phone:         out.User.Phone,
			Email:         out.User.Email,
			Password:      out.User.Password,
			Role:          int32(out.User.Role),
			Verified:      out.User.Verified,
			EmailVerified: out.User.EmailVerified,
			PhoneVerified: out.User.PhoneVerified,
		},
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u UserGRPCServer) VerifyEmail(ctx context.Context, in *pb.VerifyEmailRequest) (*pb.UserUpdateResponse, error) {
	out := u.userService.VerifyEmail(ctx, user_interface.UserVerifyEmailIn{
		UserID: int(in.GetUserID()),
	})

	return &pb.UserUpdateResponse{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u UserGRPCServer) UserChangePassword(ctx context.Context, in *pb.ChangePasswordRequest) (*pb.ChangePasswordResponse, error) {
	out := u.userService.ChangePassword(ctx, user_interface.ChangePasswordIn{
		UserID:      int(in.GetUserID()),
		OldPassword: in.GetOldPassword(),
		NewPassword: in.GetNewPassword(),
	})

	return &pb.ChangePasswordResponse{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u UserGRPCServer) GetUserByPhone(ctx context.Context, in *pb.GetByPhoneRequest) (*pb.UserOutResponse, error) {
	out := u.userService.GetByPhone(ctx, user_interface.GetByPhoneIn{
		Phone: in.GetPhone(),
	})

	return &pb.UserOutResponse{
		User: &pb.UserStruct{
			ID:            int32(out.User.ID),
			Name:          out.User.Name,
			Phone:         out.User.Phone,
			Email:         out.User.Email,
			Password:      out.User.Password,
			Role:          int32(out.User.Role),
			Verified:      out.User.Verified,
			EmailVerified: out.User.EmailVerified,
			PhoneVerified: out.User.PhoneVerified,
		},
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u UserGRPCServer) GetUserByID(ctx context.Context, in *pb.GetByIDRequest) (*pb.UserOutResponse, error) {
	out := u.userService.GetByID(ctx, user_interface.GetByIDIn{
		UserID: int(in.GetID()),
	})

	return &pb.UserOutResponse{
		User: &pb.UserStruct{
			ID:            int32(out.User.ID),
			Name:          out.User.Name,
			Phone:         out.User.Phone,
			Email:         out.User.Email,
			Password:      out.User.Password,
			Role:          int32(out.User.Role),
			Verified:      out.User.Verified,
			EmailVerified: out.User.EmailVerified,
			PhoneVerified: out.User.PhoneVerified,
		},
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (u UserGRPCServer) GetUsersByIDs(ctx context.Context, in *pb.GetByIDsRequest) (*pb.UsersOutResponse, error) {
	ids := make([]int, 0, len(in.IDs))

	for i := range in.GetIDs() {
		ids = append(ids, int(in.IDs[i]))
	}

	out := u.userService.GetByIDs(ctx, user_interface.GetByIDsIn{
		UserIDs: ids,
	})

	users := make([]*pb.UserStruct, 0, len(out.User))

	for i := range out.User {
		users = append(users, &pb.UserStruct{
			ID:            int32(out.User[i].ID),
			Name:          out.User[i].Name,
			Phone:         out.User[i].Phone,
			Email:         out.User[i].Email,
			Password:      out.User[i].Password,
			Role:          int32(out.User[i].Role),
			Verified:      out.User[i].Verified,
			EmailVerified: out.User[i].EmailVerified,
			PhoneVerified: out.User[i].PhoneVerified,
		})
	}

	return &pb.UsersOutResponse{
		User:      users,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}
